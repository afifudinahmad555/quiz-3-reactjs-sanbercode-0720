import React from 'react';
import WebPage from './quiz3/index'

function App() {
  return (
    <div className="App">
      <WebPage />
    </div>
  );
}

export default App;

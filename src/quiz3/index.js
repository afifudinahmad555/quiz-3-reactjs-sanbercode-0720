import React, {Component} from 'react';
import './public/css/style.css';
import Article from './articleIndex';
import Footer from './footerIndex';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Home from './home';
import About from './about';
import logo from './public/img/logo.png';


class Page extends Component {
    render () {
        return (
            <>
                <Router>               
                    <header> 
                        <img alt="logo" id="logo" src={logo} width={200} />
                            <nav>
                                <ul>
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/about">About</Link></li>
                                <li><Link to="/movielist">Movie List Editor</Link></li>
                                <li><Link to="/login">Login</Link></li>
                                </ul>
                            </nav>
                    </header>
              
                    <body>
                        <Switch>
                            <Route exact path="/">
                                <Home />
                            </Route>

                            <Route path="/about">
                                <About />
                            </Route>
                            
                            <Route path="/movielist">
                                <Article />
                            </Route>

                            <Route path="/login">
                                <Home />
                            </Route>    
                        </Switch>
                    </body>
                    <Footer />
                </Router>
            </>
        )
    }
}

export default Page;
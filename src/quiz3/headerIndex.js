import React, {Component} from 'react';
import './public/css/style.css';
import logo from './public/img/logo.png';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import About from './about';
import Home from './home';
import Article from './articleIndex';

class NavBar extends Component {
    render() {
        return (
            <>
                <Router>
                    <header> 
                        <img alt="logo" id="logo" src={logo} width={200} />
                            <nav>
                                <ul>
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/about">About</Link></li>
                                <li><Link to="/movielist">Movie List Editor</Link></li>
                                <li><Link to="/login">Login</Link></li>
                                </ul>
                            </nav>
                    </header>
                </Router>
            </>
        )
    }
}

export default NavBar;
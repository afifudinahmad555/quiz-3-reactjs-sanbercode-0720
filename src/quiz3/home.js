import React, {Component, useState, useEffect} from 'react';
import axios from 'axios';
import './public/css/style.css';
import pattern from './public/img/pattern.jpg';

const Home = () => {
    const[movie, setMovie] = useState(null)

    useEffect( () => {
        if (movie === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setMovie(res.data.map(el=>{ return {id: el.id, title: el.title, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating }} ))
          })
        }
      }, [movie])

    return (
        <>
        <section>
                <h1>Daftar Film Film Terbaik</h1>
                <div id="article-list">
                    <div>
                    {
                        movie !== null && movie.map((item, index)=>{
                            return(      
                                <div key={index}>              
                                    <h3><a href="">{item.title}</a></h3>
                                    <p><strong>Rating: </strong>{item.rating}
                                    <br/><strong>Durasi: </strong>{item.duration} menit
                                    <br/><strong>Genre: </strong>{item.genre}</p>
                                    <p><strong>deskripsi:</strong> {item.description}</p>
                                </div>
                            )
                        })
                    }
                    </div>
                </div>
            </section>
        </>
    )
}

export default Home;
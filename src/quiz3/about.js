import React, {Component} from 'react';

class About extends Component {
    render () {
        return (
            <div style={{padding: 10, margin: 80, border: "1px solid #ccc", background:'white'}}>
                <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <ol>
                    <li><strong style={{width: 100}}>Nama:</strong> Afifudin Ahmad</li> 
                    <li><strong style={{width: 100}}>Email:</strong> afifudinahmad555@gmail.com</li> 
                    <li><strong style={{width: 100}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                    <li><strong style={{width: 100}}>Akun Gitlab:</strong> afifudinahmad555</li> 
                    <li><strong style={{width: 100}}>Akun Telegram:</strong> afifudin_ahmad</li> 
                </ol>
            </div>
        )
    }
}

export default About
import React, {Component} from 'react';
import './public/css/style.css';

class Footer extends Component {
    render() {
        return (
            <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
            </footer>
        )
    }
}

export default Footer;
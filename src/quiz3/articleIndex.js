import React, {Component, useState, useEffect} from 'react';
import axios from 'axios';
import './public/css/style.css';
import pattern from './public/img/pattern.jpg';

const Article = () => {
    const[movie, setMovie] = useState(null)
    const [input, setInput]  =  useState({title: "", description: "", year:2000 , duration: 0, genre:"", rating: 1})
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    useEffect( () => {
        if (movie === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setMovie(res.data.map(el=>{ return {id: el.id, title: el.title, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating }} ))
          })
        }
      }, [movie])

    const handleDelete = (event) => {
        let ID_MOVIES = parseInt(event.target.value)
    
        let newmovie = movie.filter(el => el.id !== ID_MOVIES)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${ID_MOVIES}`)
        .then(res => {
          console.log(res)
        })
              
        setMovie([...newmovie])
        
    }

    const handleEdit = (event) =>{
        let ID_MOVIES = parseInt(event.target.value)
        let movie = movie.find(x=> x.id === ID_MOVIES)
        setInput({title: movie.title, description: movie.description, year: movie.year, duration: movie.duration, genre: movie.genre, rating: movie.rating})
        setSelectedId(ID_MOVIES)
        setStatusForm("edit")
    }

    const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "title":
          {
            setInput({...input, title: event.target.value});
            break
          }
          case "description":
          {
            setInput({...input, description: event.target.value});
            break
          }
          case "year":
          {
            setInput({...input, year: event.target.value});
              break
          }
          case "duration":
          {
            setInput({...input, duration: event.target.value});
              break
          }
          case "genre":
          {
            setInput({...input, genre: event.target.value});
              break
          }
          case "rating":
          {
            setInput({...input, rating: event.target.value});
              break
          }
        default:
          {break;}
        }
    }

    const handleSubmit = (event) =>{
        // menahan submit
        event.preventDefault()
    
        let title = input.title
        // let price = input.price.toString()
        let description = input.description
        let year = input.year
        let duration = input.duration
        let genre = input.genre
        let rating = input.rating
        
    
        if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" && genre.replace(/\s/g,'') !== "" && (rating >= 1 && rating <=10) ){     
            if (statusForm === "create"){        
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating})
                .then(res => {
                    setMovie([
                    ...movie, 
                    { id: res.data.id, 
                        title: input.title, 
                        description: input.description,
                        year: input.year,
                        duration: input.duration,
                        genre: input.genre,
                        rating: input.rating
                    }])
            })
            }else if(statusForm === "edit"){
                axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating})
                .then(() => {
                    let movie = movie.find(el=> el.id === selectedId)
                    movie.title = input.title
                    movie.description = input.description
                    movie.year = input.year
                    movie.duration = input.duration
                    movie.genre = input.genre
                    movie.rating = input.rating
                    setMovie([...movie])
                })
            }
          
          setStatusForm("create")
          setSelectedId(0)
          setInput({title: "", description: "", year:2000, duration: 0, genre:"", rating: 1})
        }
    
    }

    return (
        <>
        <section>
                <h1>Daftar Film Film Terbaik</h1>
                <div id="article-list">
                    <div>
                    {
                        movie !== null && movie.map((item, index)=>{
                            return(      
                                <div key={index}>              
                                    <h3><a href="">{item.title}</a></h3>
                                    <p><strong>Rating: </strong>{item.rating}
                                    <br/><strong>Durasi: </strong>{item.duration} menit
                                    <br/><strong>Genre: </strong>{item.genre}</p>
                                    <p><strong>deskripsi:</strong> {item.description}</p>
                                    <button onClick={handleEdit} value={item.id}>Edit</button>
                                    &nbsp;
                                    <button onClick={handleDelete} value={item.id}>Delete</button>
                                </div>
                            )
                        })
                    }
                    </div>
                </div>
            </section>

            <section>
            <h1>Form Movie</h1>
                <div style={{width: "50%", margin: "0 auto", display: "block"}}>
                <div style={{border: "1px solid #aaa", padding: "20px"}}>
                    <form onSubmit={handleSubmit}>
                    <label style={{float: "left"}}>
                        Title:
                    </label>
                    <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Rating (1-10):
                    </label>
                    <input style={{float: "right"}} type="number" name="rating" value={input.rating} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Durasi (dalam menit):
                    </label>
                    <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Year:
                    </label>
                    <input style={{float: "right"}} type="number" name="year" value={input.year} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Genre:
                    </label>
                    <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                        Deskripsi:
                    </label>
                    <textarea style={{float: "right", width: 170}} type="text" name="description" value={input.description} onChange={handleChange}/>
                    {/* <input /> */}
                    <br/>
                    <br/>
                    <div style={{width: "100%", paddingBottom: "20px"}}>
                        <button style={{ float: "right"}}>submit</button>
                    </div>
                    </form>
                </div>
                </div>
            </section>
        </>
    )
}

export default Article;